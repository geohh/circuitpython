//
// Created by DEVNET\hutchinsong on 6/10/19.
//

#include "shared-bindings/arf/ARFIn.h"

#include <stdint.h>

#include "lib/utils/context_manager_helpers.h"
#include "py/objproperty.h"
#include "py/runtime.h"

#include "shared-bindings/microcontroller/Pin.h"
#include "shared-bindings/util.h"
#include "supervisor/shared/translate.h"



STATIC mp_obj_t arf_arfin_make_new(const mp_obj_type_t *type, mp_uint_t n_args, const mp_obj_t *args, mp_map_t *kw_args) {
    // check arguments
    mp_arg_check_num(n_args, kw_args, 1, 1, false);

    assert_pin(args[0], false);
    const mcu_pin_obj_t *pin = MP_OBJ_TO_PTR(args[0]);

    arf_arfin_obj_t *self = m_new_obj(arf_arfin_obj_t);
    self->base.type = &arf_arfin_type;
    assert_pin_free(pin);
    common_hal_arf_arfin_construct(self, pin);

    return MP_OBJ_FROM_PTR(self);
}

STATIC mp_obj_t arf_arfin_deinit(mp_obj_t self_in) {
    arf_arfin_obj_t *self = MP_OBJ_TO_PTR(self_in);
    common_hal_arf_arfin_deinit(self);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(arf_arfin_deinit_obj, arf_arfin_deinit);

STATIC mp_obj_t arf_arfin_obj___exit__(size_t n_args, const mp_obj_t *args) {
    (void)n_args;
    common_hal_arf_arfin_deinit(args[0]);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(arf_arfin___exit___obj, 4, 4, arf_arfin_obj___exit__);



STATIC mp_obj_t arf_arfin_obj_recv(mp_obj_t self_in, mp_obj_t wave) {
    arf_arfin_obj_t *self = MP_OBJ_TO_PTR(self_in);
    raise_error_if_deinited(common_hal_arf_arfin_deinited(self));

    mp_buffer_info_t bufinfo;
    mp_get_buffer_raise(wave, &bufinfo, MP_BUFFER_WRITE);
    if (bufinfo.typecode != 'H') {
        mp_raise_TypeError(translate("Array must contain halfwords (type 'H')"));
    }
    common_hal_arf_arfin_recv(self, (uint16_t *)bufinfo.buf, bufinfo.len / 2);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(arf_arfin_recv_obj, arf_arfin_obj_recv);


STATIC mp_obj_t arf_arfin_obj_set_sampling_period(mp_obj_t self_in, mp_obj_t value) {
    arf_arfin_obj_t *self = MP_OBJ_TO_PTR(self_in);
    unsigned int v = mp_obj_get_int(value);
//    if (v < 0 || v >= 1<<16) {
//        mp_raise_ValueError(translate("Sampling period is 16 bits."));
//    }
    common_hal_arf_arfin_set_sampling_period(self, v);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(arf_arfin_set_sampling_period_obj, arf_arfin_obj_set_sampling_period);

const mp_obj_property_t arf_arfin_sampling_period_obj = {
    .base.type = &mp_type_property,
    .proxy = {(mp_obj_t)&mp_const_none_obj,
              (mp_obj_t)&arf_arfin_set_sampling_period_obj,
              (mp_obj_t)&mp_const_none_obj},
};

STATIC mp_obj_t arf_arfin_obj_get_dirty(mp_obj_t self_in) {
    arf_arfin_obj_t *self = MP_OBJ_TO_PTR(self_in);
    return mp_obj_new_bool(common_hal_arf_arfin_get_dirty(self));
}
MP_DEFINE_CONST_FUN_OBJ_1(arf_arfin_get_dirty_obj, arf_arfin_obj_get_dirty);

const mp_obj_property_t arf_arfin_dirty_obj = {
        .base.type = &mp_type_property,
        .proxy = {(mp_obj_t) &arf_arfin_get_dirty_obj,
                  (mp_obj_t) &mp_const_none_obj,
                  (mp_obj_t) &mp_const_none_obj,
                  },
};

//|   .. attribute:: reference_voltage
//|
//|     The maximum voltage measurable (also known as the reference voltage) as a
//|     `float` in Volts.
//|
STATIC mp_obj_t arf_arfin_obj_get_reference_voltage(mp_obj_t self_in) {
    arf_arfin_obj_t *self = MP_OBJ_TO_PTR(self_in);
    raise_error_if_deinited(common_hal_arf_arfin_deinited(self));
    return mp_obj_new_float(common_hal_arf_arfin_get_reference_voltage(self));
}
MP_DEFINE_CONST_FUN_OBJ_1(arf_arfin_get_reference_voltage_obj,
                          arf_arfin_obj_get_reference_voltage);

const mp_obj_property_t arf_arfin_reference_voltage_obj = {
    .base.type = &mp_type_property,
    .proxy = {(mp_obj_t)&arf_arfin_get_reference_voltage_obj,
              (mp_obj_t)&mp_const_none_obj,
              (mp_obj_t)&mp_const_none_obj},
};


STATIC const mp_rom_map_elem_t arf_arfin_locals_dict_table[] = {
    // Methods
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&arf_arfin_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR___enter__), MP_ROM_PTR(&default___enter___obj) },
    { MP_ROM_QSTR(MP_QSTR___exit__), MP_ROM_PTR(&arf_arfin___exit___obj) },
    { MP_ROM_QSTR(MP_QSTR_recv), MP_ROM_PTR(&arf_arfin_recv_obj) },
    //Properties
    { MP_OBJ_NEW_QSTR(MP_QSTR_sampling_period), (mp_obj_t)&arf_arfin_sampling_period_obj },
    { MP_OBJ_NEW_QSTR(MP_QSTR_dirty), (mp_obj_t)&arf_arfin_dirty_obj },
};
STATIC MP_DEFINE_CONST_DICT(arf_arfin_locals_dict, arf_arfin_locals_dict_table);

const mp_obj_type_t arf_arfin_type = {
    { &mp_type_type },
    .name = MP_QSTR_ARFIn,
    .make_new = arf_arfin_make_new,
    .locals_dict = (mp_obj_dict_t*)&arf_arfin_locals_dict,
};



