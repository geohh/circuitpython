//
// Created by DEVNET\hutchinsong on 6/5/19.
//

#ifndef MICROPY_INCLUDED_SHARED_BINDINGS_ARF_ARFOUT_H
#define MICROPY_INCLUDED_SHARED_BINDINGS_ARF_ARFOUT_H

#include "common-hal/arf/ARFOut.h"
#include "common-hal/microcontroller/Pin.h"

extern const mp_obj_type_t arf_arfout_type;

extern void common_hal_arf_arfout_construct(arf_arfout_obj_t* self, const mcu_pin_obj_t *pin);
extern void common_hal_arf_arfout_deinit(arf_arfout_obj_t* self);
extern bool common_hal_arf_arfout_deinited(arf_arfout_obj_t* self);
extern void common_hal_arf_arfout_send(arf_arfout_obj_t* self,
    uint16_t* wave, uint16_t len);
extern void common_hal_arf_arfout_set_sampling_period(arf_arfout_obj_t* self, unsigned int value);

#endif //CIRCUITPYTHON_DSP_ARFOUT_H
