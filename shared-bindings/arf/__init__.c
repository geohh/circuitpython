//TODO: License info?
//

#include "py/obj.h"
#include "py/runtime.h"

#include "shared-bindings/microcontroller/Pin.h"
#include "shared-bindings/arf/__init__.h"
#include "shared-bindings/arf/ARFOut.h"
#include "shared-bindings/arf/ARFIn.h"


//| :mod: `arf` --- Analog Radio-Frequency I/O
//| ==========================================
//TODO: More complete documentation
//
//


STATIC const mp_rom_map_elem_t arf_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_arf) },
    { MP_ROM_QSTR(MP_QSTR_ARFOut),   MP_ROM_PTR(&arf_arfout_type) },
    { MP_ROM_QSTR(MP_QSTR_ARFIn),    MP_ROM_PTR(&arf_arfin_type)  },
};

STATIC MP_DEFINE_CONST_DICT(arf_module_globals, arf_module_globals_table);

const mp_obj_module_t arf_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&arf_module_globals,
};

