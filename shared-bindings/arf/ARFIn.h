
#ifndef MICROPY_INCLUDED_SHARED_BINDINGS_ARF_ARFIN_H
#define MICROPY_INCLUDED_SHARED_BINDINGS_ARF_ARFIN_H

#include "common-hal/arf/ARFIn.h"
#include "common-hal/microcontroller/Pin.h"

extern const mp_obj_type_t arf_arfin_type;

extern void common_hal_arf_arfin_construct(arf_arfin_obj_t* self, const mcu_pin_obj_t *pin);
extern void common_hal_arf_arfin_deinit(arf_arfin_obj_t* self);
extern bool common_hal_arf_arfin_deinited(arf_arfin_obj_t* self);
extern void common_hal_arf_arfin_recv(arf_arfin_obj_t* self,
    uint16_t* wave, uint16_t len);
extern void common_hal_arf_arfin_set_sampling_period(arf_arfin_obj_t *self, unsigned int value);
extern bool common_hal_arf_arfin_get_dirty(arf_arfin_obj_t *self);
extern float common_hal_arf_arfin_get_reference_voltage(arf_arfin_obj_t *self);

#endif //CIRCUITPYTHON_DSP_ARFOUT_H
