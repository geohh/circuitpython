//
// Created by DEVNET\hutchinsong on 6/5/19.
//

#include <stdint.h>

#include "lib/utils/context_manager_helpers.h"
#include "py/objproperty.h"
#include "py/runtime.h"

#include "shared-bindings/microcontroller/Pin.h"
#include "shared-bindings/arf/ARFOut.h"
#include "shared-bindings/util.h"
#include "supervisor/shared/translate.h"

STATIC mp_obj_t arf_arfout_make_new(const mp_obj_type_t *type, mp_uint_t n_args, const mp_obj_t *args, mp_map_t *kw_args) {
    // check arguments
    mp_arg_check_num(n_args, kw_args, 1, 1, false);

    assert_pin(args[0], false);
    const mcu_pin_obj_t *pin = MP_OBJ_TO_PTR(args[0]);

    arf_arfout_obj_t *self = m_new_obj(arf_arfout_obj_t);
    self->base.type = &arf_arfout_type;
    assert_pin_free(pin);
    common_hal_arf_arfout_construct(self, pin);

    return MP_OBJ_FROM_PTR(self);
}

STATIC mp_obj_t arf_arfout_deinit(mp_obj_t self_in) {
    arf_arfout_obj_t *self = MP_OBJ_TO_PTR(self_in);
    common_hal_arf_arfout_deinit(self);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(arf_arfout_deinit_obj, arf_arfout_deinit);

STATIC mp_obj_t arf_arfout_obj___exit__(size_t n_args, const mp_obj_t *args) {
    (void)n_args;
    common_hal_arf_arfout_deinit(args[0]);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(arf_arfout___exit___obj, 4, 4, arf_arfout_obj___exit__);

STATIC mp_obj_t arf_arfout_obj_send(mp_obj_t self_in, mp_obj_t wave) {
    arf_arfout_obj_t *self = MP_OBJ_TO_PTR(self_in);
    raise_error_if_deinited(common_hal_arf_arfout_deinited(self));

    mp_buffer_info_t bufinfo;
    mp_get_buffer_raise(wave, &bufinfo, MP_BUFFER_READ);
    if (bufinfo.typecode != 'H') {
        mp_raise_TypeError(translate("Array must contain halfwords (type 'H')"));
    }
    common_hal_arf_arfout_send(self, (uint16_t *)bufinfo.buf, bufinfo.len / 2);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(arf_arfout_send_obj, arf_arfout_obj_send);


STATIC mp_obj_t arf_arfout_obj_set_sampling_period(mp_obj_t self_in, mp_obj_t value) {
    arf_arfout_obj_t *self = MP_OBJ_TO_PTR(self_in);
    unsigned int v = mp_obj_get_int(value);

    if (v < 0) {
        mp_raise_ValueError(translate("Sampling period is restricted to unsigned 32 bit integers."));
    }
    
    common_hal_arf_arfout_set_sampling_period(self, v);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(arf_arfout_set_sampling_period_obj, arf_arfout_obj_set_sampling_period);

STATIC mp_obj_t arf_arfout_obj_get_sampling_period(mp_obj_t self_in) {
    arf_arfout_obj_t *self = MP_OBJ_TO_PTR(self_in);
    unsigned int v = self->cb->sampling_period;
    mp_obj_t value = mp_obj_new_int(v);
    return value;
}
MP_DEFINE_CONST_FUN_OBJ_1(arf_arfout_get_sampling_period_obj, arf_arfout_obj_get_sampling_period);

const mp_obj_property_t arf_arfout_sampling_period_obj = {
    .base.type = &mp_type_property,
    .proxy = {(mp_obj_t)&arf_arfout_get_sampling_period_obj,
              (mp_obj_t)&arf_arfout_set_sampling_period_obj,
              (mp_obj_t)&mp_const_none_obj},
};

STATIC const mp_rom_map_elem_t arf_arfout_locals_dict_table[] = {
    // Methods
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&arf_arfout_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR___enter__), MP_ROM_PTR(&default___enter___obj) },
    { MP_ROM_QSTR(MP_QSTR___exit__), MP_ROM_PTR(&arf_arfout___exit___obj) },
    { MP_ROM_QSTR(MP_QSTR_send), MP_ROM_PTR(&arf_arfout_send_obj) },
    //Properties
    { MP_OBJ_NEW_QSTR(MP_QSTR_sampling_period), (mp_obj_t)&arf_arfout_sampling_period_obj },
};
STATIC MP_DEFINE_CONST_DICT(arf_arfout_locals_dict, arf_arfout_locals_dict_table);

const mp_obj_type_t arf_arfout_type = {
    { &mp_type_type },
    .name = MP_QSTR_ARFOut,
    .make_new = arf_arfout_make_new,
    .locals_dict = (mp_obj_dict_t*)&arf_arfout_locals_dict,
};




