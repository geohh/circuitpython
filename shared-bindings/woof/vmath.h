#ifndef MICROPY_INCLUDED_SHARED_BINDINGS_WOOF_VMATH_H
#define MICROPY_INCLUDED_SHARED_BINDINGS_WOOF_VMATH_H

#include "py/obj.h"
#include "py/runtime.h"

extern const mp_obj_module_t woof_vmath_module;

#endif
