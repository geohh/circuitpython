#include "shared-bindings/woof/vmath.h"

#include "shared-bindings/woof/__init__.h"

#include <stdint.h>

#include "py/obj.h"
#include "py/objarray.h"
#include "py/runtime.h"
#include "py/mperrno.h"

#include "supervisor/shared/translate.h"
#include "shared-bindings/microcontroller/Pin.h" //this pulls in the MCU's header file,
                                                 //which will override the __FPU_PRESENT
                                                 //set to 0 in core_xxx.h pulled in by arm_math.h


#include "arm_math.h"
#include "arm_const_structs.h"

STATIC mp_obj_t woof_vmath_vsadd(size_t n_args, const mp_obj_t *args) {
    if (n_args != 4) {
        mp_raise_ValueError(translate("woof_vmath_vsaddrr recieved improper argument array."));
    }
    mp_obj_t output_vector = args[0];
    mp_obj_t source_vector = args[1];
    mp_obj_t source_scalar = args[2];
    mp_obj_t datatype_in = args[3];
    mp_buffer_info_t bufin, bufout;
    mp_get_buffer_raise(source_vector, &bufin, MP_BUFFER_READ);
    mp_get_buffer_raise(output_vector, &bufout, MP_BUFFER_WRITE);
    enum woof_datatype datatype = mp_obj_get_int(datatype_in);
    switch (datatype) {
        case woof_float32:
            arm_offset_f32((float32_t*) bufin.buf, mp_obj_get_float(source_scalar), (float32_t*) bufout.buf, bufin.len / sizeof(float32_t));
            break;
        //add int, uint are same. collapse?
        case woof_uint32:
            arm_offset_q31((q31_t*) bufin.buf, mp_obj_get_int(source_scalar), (q31_t*) bufout.buf, bufin.len / sizeof(q31_t));
            break;
        case woof_int32:
            arm_offset_q31((q31_t*) bufin.buf, mp_obj_get_float(source_scalar), (q31_t*) bufout.buf, bufin.len / sizeof(q31_t));
            break;
        default:
            mp_raise_TypeError(translate("This operation (vsadd) does not support this datatype"));
            break;
    }

    return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_VAR(woof_vmath_vsadd_obj, 4, woof_vmath_vsadd);

STATIC const mp_rom_map_elem_t woof_vmath_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_vmath) },
    { MP_ROM_QSTR(MP_QSTR_vsadd), MP_ROM_PTR(&woof_vmath_vsadd_obj) },
};

STATIC MP_DEFINE_CONST_DICT(woof_vmath_module_globals, woof_vmath_globals_table);

const mp_obj_module_t woof_vmath_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&woof_vmath_module_globals,
};
