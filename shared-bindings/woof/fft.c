



#include "shared-bindings/woof/__init__.h"

#include "shared-module/woof/fft.h"

#include <stdint.h>

#include "py/obj.h"
#include "py/objarray.h"
#include "py/runtime.h"
#include "py/mperrno.h"


#include "supervisor/shared/translate.h"
#include "shared-bindings/microcontroller/Pin.h" //this pulls in the MCU's header file,
                                                 //which will override the __FPU_PRESENT
                                                 //set to 0 in core_xxx.h pulled in by arm_math.h

#include "arm_math.h"
#include "arm_const_structs.h"


STATIC mp_obj_t woof_fft_rfft(mp_obj_t Signal_out, mp_obj_t Signal_in) {
    mp_buffer_info_t bufin, bufout;
    mp_get_buffer_raise(Signal_in, &bufin, MP_BUFFER_READ);
    mp_get_buffer_raise(Signal_out, &bufout, MP_BUFFER_WRITE);
    switch (bufin.typecode) {
        case 'f':
            shared_module_woof_fft_rfft_float32((float32_t *)bufin.buf, (float32_t *)bufout.buf, bufin.len / sizeof(float32_t));
            break;
        case 'h':
            shared_module_woof_fft_rfft_q15((q15_t *)bufin.buf, (q15_t *)bufout.buf, bufin.len / sizeof(q15_t));
            break;
        case 'H':
            shared_module_woof_fft_rfft_uint16((uint16_t *)bufin.buf, (uint16_t *)bufout.buf, bufin.len / sizeof(uint16_t));
            break;
         default:
            mp_raise_ValueError(translate("input signal is of an unsupported type."));
            break;
    }

    return Signal_out;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(woof_fft_rfft_obj, woof_fft_rfft);

STATIC mp_obj_t woof_fft_cfft(mp_obj_t Signal_out, mp_obj_t Signal_in) {
    mp_buffer_info_t bufin, bufout;
    mp_get_buffer_raise(Signal_in, &bufin, MP_BUFFER_READ);
    mp_get_buffer_raise(Signal_out, &bufout, MP_BUFFER_WRITE);
    switch (bufin.typecode) {
        case 'f':
            shared_module_woof_fft_cfft_float32((float32_t *)bufin.buf, (float32_t *)bufout.buf, bufin.len / sizeof(float32_t) / 2);
            break;
          default:
            mp_raise_ValueError(translate("input signal is of an unsupported type."));
            break;
    }

    return Signal_out;

}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(woof_fft_cfft_obj, woof_fft_cfft);


STATIC const mp_rom_map_elem_t woof_fft_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_fft) },
    { MP_ROM_QSTR(MP_QSTR_rfft), MP_ROM_PTR(&woof_fft_rfft_obj) },
    { MP_ROM_QSTR(MP_QSTR_cfft), MP_ROM_PTR(&woof_fft_cfft_obj) },
};

STATIC MP_DEFINE_CONST_DICT(woof_fft_module_globals, woof_fft_module_globals_table);

const mp_obj_module_t woof_fft_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&woof_fft_module_globals,
};

