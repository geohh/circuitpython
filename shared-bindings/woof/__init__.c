#include "shared-bindings/woof/__init__.h"

#include "shared-bindings/woof/fft.h"
#include "shared-bindings/woof/vmath.h"

#include "py/obj.h"
#include "py/runtime.h"

//Raising datatypes enumeration to python
//const woof_datatype_obj_t woof_datatype_float32 = {
//    .base = &mp_type_type,
//    .value = woof_float32,
//};


STATIC const mp_rom_map_elem_t woof_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_woof) },
    { MP_ROM_QSTR(MP_QSTR_fft), MP_ROM_PTR(&woof_fft_module) },
    { MP_ROM_QSTR(MP_QSTR_vmath), MP_ROM_PTR(&woof_vmath_module) },
    //Datatypes
    { MP_ROM_QSTR(MP_QSTR_float32),   MP_ROM_INT(woof_float32)},
    { MP_ROM_QSTR(MP_QSTR_int16),     MP_ROM_INT(woof_int16) },
    { MP_ROM_QSTR(MP_QSTR_uint16),    MP_ROM_INT(woof_uint16) },
    { MP_ROM_QSTR(MP_QSTR_int32),     MP_ROM_INT(woof_int32) },
    { MP_ROM_QSTR(MP_QSTR_uint32),    MP_ROM_INT(woof_uint32) },
    { MP_ROM_QSTR(MP_QSTR_intc),      MP_ROM_INT(woof_intc) },
    { MP_ROM_QSTR(MP_QSTR_uintc),     MP_ROM_INT(woof_uintc) },
};

STATIC MP_DEFINE_CONST_DICT(woof_module_globals, woof_module_globals_table);

const mp_obj_module_t woof_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&woof_module_globals,
};

