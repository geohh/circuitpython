#ifndef MICROPY_INCLUDED_SHARED_BINDINGS_WOOF___INIT__H
#define MICROPY_INCLUDED_SHARED_BINDINGS_WOOF___INIT__H
#include "py/obj.h"

#include <stdint.h>


enum woof_datatype { woof_float32, woof_int16, woof_uint16, woof_int32, woof_uint32, woof_intc, woof_uintc};

typedef struct {
    mp_obj_base_t base;
    enum woof_datatype value;
} woof_datatype_obj_t;

#endif
