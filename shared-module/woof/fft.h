#ifndef MICROPY_WOOF_INCLUDED_SHARED_MODULE_FFT_H
#define MICROPY_WOOF_INCLUDED_SHARED_MODULE_FFT_H


#include <stdint.h>

#include "shared-bindings/microcontroller/Pin.h"

#include "arm_math.h"

void shared_module_woof_fft_rfft_float32(float32_t *data_in, float32_t *data_out, unsigned short fft_len);
void shared_module_woof_fft_rfft_q15(q15_t *data_in, q15_t *data_out, unsigned short fft_len);
void shared_module_woof_fft_rfft_uint16(uint16_t *data_in, uint16_t *data_out, unsigned short fft_len);
void shared_module_woof_fft_cfft_float32(float32_t *data_in, float32_t *data_out, unsigned short fft_len);

#endif
