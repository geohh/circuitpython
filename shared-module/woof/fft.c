#include "shared-module/woof/fft.h"

#include <stdint.h>

#include "py/obj.h"
#include "py/objarray.h"
#include "py/runtime.h"
#include "py/mperrno.h"


#include "supervisor/shared/translate.h"
#include "shared-bindings/microcontroller/Pin.h"

#include "arm_math.h"
#include "arm_const_structs.h"

void shared_module_woof_fft_rfft_float32(float32_t *data_in, float32_t *data_out, unsigned short fft_len) {
    float32_t data_scratch[fft_len * 2];
    for (unsigned short i=0; i < fft_len; i++) {
        data_scratch[(2*i)+0] = data_in[i];
        data_scratch[(2*i)+1] = 0.0;
    }

    const arm_cfft_instance_f32 *fft_scratch;
    switch (fft_len) {
        case 16:
            fft_scratch = &arm_cfft_sR_f32_len16;
            break; 
        case 32:
            fft_scratch = &arm_cfft_sR_f32_len32;
            break;
        case 64:
            fft_scratch = &arm_cfft_sR_f32_len64;
            break;
        case 128:
            fft_scratch = &arm_cfft_sR_f32_len128;
            break;
        case 512:
            fft_scratch = &arm_cfft_sR_f32_len512;
            break;
        case 1024:
            fft_scratch = &arm_cfft_sR_f32_len1024;
            break;
        default:
            mp_raise_ValueError(translate("only lengths equal to powers of 2 from 16 to 1024 are supported"));
            break;
    }
    arm_cfft_f32(fft_scratch, data_scratch, 0, 1);
    memcpy(data_out, data_scratch, sizeof(float32_t) * fft_len * 2);
}

void shared_module_woof_fft_rfft_q15(q15_t *data_in, q15_t *data_out, unsigned short fft_len) {
    q15_t data_scratch[fft_len * 2];
    for (unsigned short i=0; i < fft_len; i++) {
        data_scratch[(2*i)+0] = data_in[i];
        data_scratch[(2*i)+1] = 0;
    }

    const arm_cfft_instance_q15 *fft_scratch;
    switch (fft_len) {
        case 16:
            fft_scratch = &arm_cfft_sR_q15_len16;
            break; 
        case 32:
            fft_scratch = &arm_cfft_sR_q15_len32;
            break;
        case 64:
            fft_scratch = &arm_cfft_sR_q15_len64;
            break;
        case 128:
            fft_scratch = &arm_cfft_sR_q15_len128;
            break;
        case 512:
            fft_scratch = &arm_cfft_sR_q15_len512;
            break;
        case 1024:
            fft_scratch = &arm_cfft_sR_q15_len1024;
            break;
        default:
            mp_raise_ValueError(translate("only lengths equal to powers of 2 from 16 to 1024 are supported"));
            break;
    }
    arm_cfft_q15(fft_scratch, data_scratch, 0, 1);
    memcpy(data_out, data_scratch, sizeof(float32_t) * fft_len * 2);
}

void shared_module_woof_fft_rfft_uint16(uint16_t *data_in, uint16_t *data_out, unsigned short fft_len) {
    /* NB: woof.fft on packed unsigned time series
     * will monotonicly re-bias to signed before transforming.
     * This will result in an incorrect
     * constant offset, i.e. X[0].
     * Use at your own risk.
     */
    q15_t data_scratch[fft_len * 2];
    for (unsigned short i=0; i < fft_len; i++) {
        data_scratch[(2*i)+0] = (q15_t)(data_in[i] - (1<<15));
        data_scratch[(2*i)+1] = 0;
    }

    const arm_cfft_instance_q15 *fft_scratch;
    switch (fft_len) {
        case 16:
            fft_scratch = &arm_cfft_sR_q15_len16;
            break; 
        case 32:
            fft_scratch = &arm_cfft_sR_q15_len32;
            break;
        case 64:
            fft_scratch = &arm_cfft_sR_q15_len64;
            break;
        case 128:
            fft_scratch = &arm_cfft_sR_q15_len128;
            break;
        case 512:
            fft_scratch = &arm_cfft_sR_q15_len512;
            break;
        case 1024:
            fft_scratch = &arm_cfft_sR_q15_len1024;
            break;
        default:
            mp_raise_ValueError(translate("only lengths equal to powers of 2 from 16 to 1024 are supported"));
            break;
    }
    arm_cfft_q15(fft_scratch, data_scratch, 0, 1);
    memcpy(data_out, data_scratch, sizeof(float32_t) * fft_len * 2);
}

void shared_module_woof_fft_cfft_float32(float32_t *data_in, float32_t *data_out, unsigned short fft_len) {
    float32_t data_scratch[fft_len * 2];
    for (unsigned short i=0; i < fft_len * 2; i++) {
        data_scratch[i] = data_in[i];
    }

    const arm_cfft_instance_f32 *fft_scratch;
    switch (fft_len) {
        case 16:
            fft_scratch = &arm_cfft_sR_f32_len16;
            break; 
        case 32:
            fft_scratch = &arm_cfft_sR_f32_len32;
            break;
        case 64:
            fft_scratch = &arm_cfft_sR_f32_len64;
            break;
        case 128:
            fft_scratch = &arm_cfft_sR_f32_len128;
            break;
        case 512:
            fft_scratch = &arm_cfft_sR_f32_len512;
            break;
        case 1024:
            fft_scratch = &arm_cfft_sR_f32_len1024;
            break;
        default:
            mp_raise_ValueError(translate("only lengths equal to powers of 2 from 16 to 1024 are supported"));
            break;
    }
    arm_cfft_f32(fft_scratch, data_scratch, 0, 1);
    arm_copy_f32(data_scratch, data_out, fft_len * 2);
}


