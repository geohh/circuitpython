//
// Created by DEVNET\hutchinsong on 6/6/19.
//

#include "common-hal/arf/ARFIn.h"

#include <stdint.h>
#include <stdio.h>

#include "py/runtime.h"
#include "py/mperrno.h"
#include "py/gc.h"
#include "py/nlr.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "py/mphal.h"

#include "samd/timers.h"
#include "samd/pins.h"
#include "samd/adc.h"
#include "timer_handler.h"
#include "peripheral_clk_config.h"

#include "atmel_start_pins.h"
#include "hal/include/hal_adc_sync.h"
#include "hpl/gclk/hpl_gclk_base.h"

#include "shared-bindings/arf/ARFIn.h"


static uint8_t refcount;

//All ARFIn instances share a single timer
static uint8_t arfin_tc_index = 0xff;
//static volatile uint16_t sampling_period;

//PoC ARFIn uses a single buffer
//static uint16_t *wave_buffer = NULL;
//static volatile uint16_t sample_index = 0;
//static uint16_t wave_length;
//static volatile uint32_t current_compare = 0;
//static struct adc_sync_descriptor *gl_descriptor;
//static uint8_t gl_channel;
//static volatile bool dirty;

static struct arf_arfin_callback_info callback_table[TC_INST_NUM];

void read_value(uint16_t *value_ptr, uint8_t channel, struct adc_sync_descriptor *descriptor) {
    uint16_t value;
    adc_sync_read_channel(descriptor, channel, ((uint8_t*) &value), 2);
    *value_ptr = value << 4;
}

void finish_collect(struct arf_arfin_callback_info *cb, uint8_t index) {
    cb->dirty = false;
    Tc* tc = tc_insts[index];
    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_STOP;
    tc->COUNT16.INTENCLR.reg = TC_INTENCLR_MC0;
    tc_disable_interrupts(arfin_tc_index);
}

//use global instance descriptor for proof-of-concept
static void next_sample(uint8_t index) {
    struct arf_arfin_callback_info *cb = &callback_table[index];
    cb->sample_index++;
    if (cb->sample_index >= cb->wave_length) {
        finish_collect(cb, index);
        return;
    }
    //Interrupt control is coming from a 48MHz clock, divided by 8 = 6MHz
    //User gives \mu s / sample, so we multiply by 6
    cb->current_compare = (cb->current_compare + cb->sampling_period) & 0xffff;
    Tc* tc = tc_insts[index];
    tc->COUNT16.CC[0].reg = cb->current_compare;

    read_value(&(cb->wave_buffer[cb->sample_index]), cb->channel, &cb->descriptor);
}

void arfin_interrupt_handler(uint8_t index) {
    if (!callback_table[index].dirty) return;
    Tc* tc = tc_insts[index];
    if (!tc->COUNT16.INTFLAG.bit.MC0) return;

    next_sample(index);

    // Clear the interrupt bit.
    tc->COUNT16.INTFLAG.reg = TC_INTFLAG_MC0;
}

bool common_hal_arf_arfin_get_dirty(arf_arfin_obj_t *self) {
    return self->cb->dirty;
}

void arfin_reset(void) {
    refcount = 0;
//    arfin_tc_index = 0xff;
    for (unsigned short i = 0; i < TC_INST_NUM; i++) {
        adc_sync_deinit(&callback_table[i].descriptor);
        callback_table[i].dirty = false;
        tc_reset(tc_insts[i]);
    }
}


bool common_hal_arf_arfin_deinited(arf_arfin_obj_t *self) {
    return self->deinited;
}

void common_hal_arf_arfin_deinit(arf_arfin_obj_t *self) {
    refcount--;
    tc_reset(tc_insts[self->tc_index]);
    adc_sync_deinit(&(self->cb->descriptor));
}

void common_hal_arf_arfin_construct(arf_arfin_obj_t *self, const mcu_pin_obj_t *pin) {
        // Find a spare timer.
    Tc *tc = NULL;
    int8_t index = TC_INST_NUM - 1;
    for (; index >= 0; index--) {
        if (tc_insts[index]->COUNT16.CTRLA.bit.ENABLE == 0) {
            tc = tc_insts[index];
            break;
        }
    }
    if (tc == NULL) {
        mp_raise_RuntimeError(translate("All timers in use"));
    }

    self->tc_index = index;
    self->cb = &callback_table[index];

    set_timer_handler(true, index, TC_HANDLER_ARFIN);
    // We use GCLK0 for SAMD21 and GCLK1 for SAMD51 because they both run at 48mhz making our
    // math the same across the boards.
    #ifdef SAMD21
    turn_on_clocks(true, index, 0);
    #endif
    #ifdef SAMD51
    turn_on_clocks(true, index, 1);
    #endif


    #ifdef SAMD21
    tc->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 |
                            TC_CTRLA_PRESCALER_DIV16 |
                            TC_CTRLA_WAVEGEN_NFRQ;
    #endif
    #ifdef SAMD51
    tc_reset(tc);
    tc_set_enable(tc, false);
    tc->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT8 |
                            TC_CTRLA_PRESCALER_DIV16;
    tc->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_NFRQ;
    #endif

    tc_set_enable(tc, true);
    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_STOP;
 
    uint8_t adc_index;
    uint8_t adc_channel = 0xff;
    for (adc_index = 0; adc_index < NUM_ADC_PER_PIN; adc_index++) {
        if (pin->adc_input[adc_index] != 0xff) {
            adc_channel = pin->adc_input[adc_index];
            break;
        }
    }
    if (adc_channel == 0xff) {
        // No ADC function on that pin
        mp_raise_ValueError(translate("Pin does not have ADC capabilities"));
    }
    claim_pin(pin);

    gpio_set_pin_function(pin->number, GPIO_PIN_FUNCTION_B);

    static Adc* adc_insts[] = ADC_INSTS;
    self->instance = adc_insts[adc_index];
    self->cb->channel = adc_channel;
    samd_peripherals_adc_setup(&self->cb->descriptor, self->instance);

    // Full scale is 3.3V (VDDANA) = 65535.

    // On SAMD21, INTVCC1 is 0.5*VDDANA. On SAMD51, INTVCC1 is 1*VDDANA.
    // So on SAMD21 only, divide the input by 2, so full scale will match 0.5*VDDANA.
    adc_sync_set_reference(&self->cb->descriptor, ADC_REFCTRL_REFSEL_INTVCC1_Val);
    #ifdef SAMD21
    adc_sync_set_channel_gain(&self->cb->descriptor, self->cb->channel, ADC_INPUTCTRL_GAIN_DIV2_Val);
    #endif

    adc_sync_set_resolution(&self->cb->descriptor, ADC_CTRLB_RESSEL_12BIT_Val);

    adc_sync_enable_channel(&self->cb->descriptor, self->cb->channel);

    // We need to set the inputs because the above channel enable only enables the ADC.
    adc_sync_set_inputs(&self->cb->descriptor, self->cb->channel, ADC_INPUTCTRL_MUXNEG_GND_Val, self->cb->channel);

    self->cb->dirty = false;
    self->deinited = false;

}

void common_hal_arf_arfin_set_sampling_period(arf_arfin_obj_t *self, unsigned int value) {
    self->cb->sampling_period = value;
}


void common_hal_arf_arfin_recv(arf_arfin_obj_t* self, uint16_t* wave, uint16_t length) {
    if (self->cb->dirty) {
        mp_raise_RuntimeError(translate("This port is currently buffering data."));
    }
    self->cb->dirty = true;

    self->cb->wave_buffer = wave;
    self->cb->wave_length = length;
    self->cb->sample_index = 0;



    self->cb->current_compare = self->cb->sampling_period;
    Tc* tc = tc_insts[self->tc_index];
    tc->COUNT16.CC[0].reg = self->cb->current_compare;

    // Clear our interrupt in case it was set earlier
    tc->COUNT16.INTFLAG.reg = TC_INTFLAG_MC0;
    tc->COUNT16.INTENSET.reg = TC_INTENSET_MC0;
    tc_enable_interrupts(self->tc_index);

    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_RETRIGGER;

    read_value(&(self->cb->wave_buffer[0]), self->cb->channel, &self->cb->descriptor);
}

float common_hal_arf_arfin_get_reference_voltage(arf_arfin_obj_t *self) {
    return 3.3f;
}
