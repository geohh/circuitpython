//
// Created by DEVNET\hutchinsong on 6/5/19.
//

#include "common-hal/arf/ARFOut.h"


#include <stdint.h>
#include <stdio.h>

#include "atmel_start_pins.h"
#include "hal/include/hal_dac_sync.h"
#include "hpl/gclk/hpl_gclk_base.h"
#include "peripheral_clk_config.h"
#include "samd/timers.h"
#include "samd/pins.h"
#include "timer_handler.h"

#include "py/mperrno.h"
#include "py/gc.h"
#include "py/runtime.h"

#include "shared-bindings/audioio/AudioOut.h"
#include "shared-bindings/microcontroller/Pin.h"

static uint8_t refcount = 0;
//All ARFOut instances share a single timer
//static uint8_t arfout_tc_index = 0xff;
//static uint16_t sampling_period = 1000;

static volatile struct callback_info callback_table[TC_INST_NUM];


// Proof-of-concept ARF supports only a single output at a time
//static uint16_t *wave_buffer = NULL;
//static volatile uint16_t sample_index = 0;
//static uint16_t wave_length;
//static volatile uint32_t current_compare = 0;
//static struct dac_sync_descriptor *gl_descriptor;
//static uint8_t gl_channel;

void write_value(uint16_t value, volatile struct callback_info *cb) {
    #if defined(SAMD21) && !defined(PIN_PA02)
    return;
    #endif
    // Input is 16 bit so make sure and set LEFTADJ to 1 so it takes the top
    // bits. This is currently done in asf4_conf/*/hpl_dac_config.h.
    dac_sync_write((struct dac_sync_descriptor *) &(cb->descriptor), cb->channel, &value, 1);
}



void next_sample(uint8_t index) {
    volatile struct callback_info *cb = &callback_table[index];
    cb->sample_index++;
    if (cb->sample_index >= cb->wave_length) {
        cb->sample_index = 0;
    }
    //Interrupt control is coming from a 48MHz clock, divided by 16 = 3MHz
    cb->current_compare = (cb->current_compare + cb->sampling_period) & 0xffff;
    Tc* tc = tc_insts[index];
    tc->COUNT16.CC[0].reg = cb->current_compare;
    write_value((cb->wave_buffer)[cb->sample_index], cb);

}


void arfout_interrupt_handler(uint8_t index) {
    if (callback_table[index].active != true) return;
    Tc* tc = tc_insts[index];
    if (!tc->COUNT16.INTFLAG.bit.MC0) return;

    next_sample(index);

    // Clear the interrupt bit.
    tc->COUNT16.INTFLAG.reg = TC_INTFLAG_MC0;
}

void arfout_reset(void) {
    refcount = 0;
    // audioout_reset also resets the DAC, and does a smooth ramp down to avoid clicks
    // if it was enabled, so do that instead if AudioOut is enabled.
    #if CIRCUITPY_AUDIOIO
       audioout_reset();
    #else
       #ifdef SAMD21
       while (DAC->STATUS.reg & DAC_STATUS_SYNCBUSY) {}
       #endif
       #ifdef SAMD51
       while (DAC->SYNCBUSY.reg & DAC_SYNCBUSY_SWRST) {}
       #endif
       DAC->CTRLA.reg |= DAC_CTRLA_SWRST;
    #endif
}

void common_hal_arf_arfout_construct(arf_arfout_obj_t *self, const mcu_pin_obj_t *pin) {
    // Find a spare timer.
    Tc *tc = NULL;
    int8_t index = TC_INST_NUM - 1;
    for (; index >= 0; index--) {
        if (tc_insts[index]->COUNT16.CTRLA.bit.ENABLE == 0) {
            tc = tc_insts[index];
            break;
        }
    }
    if (tc == NULL) {
        mp_raise_RuntimeError(translate("All timers in use"));
    }

    self->tc_index = index;

    set_timer_handler(true, index, TC_HANDLER_ARFOUT);
    // We use GCLK0 for SAMD21 and GCLK1 for SAMD51 because they both run at 48mhz making our
    // math the same across the boards.
    #ifdef SAMD21
    turn_on_clocks(true, index, 0);
    #endif
    #ifdef SAMD51
    turn_on_clocks(true, index, 1);
    #endif


    #ifdef SAMD21
    tc->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 |
                            TC_CTRLA_PRESCALER_DIV16 |
                            TC_CTRLA_WAVEGEN_NFRQ;
    #endif
    #ifdef SAMD51
    tc_reset(tc);
    tc_set_enable(tc, false);
    tc->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 |
                            TC_CTRLA_PRESCALER_DIV16;
    tc->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_NFRQ;
    #endif

    tc_set_enable(tc, true);
    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_STOP;

    refcount++;
    
    struct callback_info *cb = ((struct callback_info *) callback_table) + index;
    self->cb = cb;


    // Take ownership of DAC and output pin
    #if defined(SAMD21) && !defined(PIN_PA02)
    mp_raise_NotImplementedError(translate("No DAC on chip"));
    #else
    if (pin->number != PIN_PA02
    #ifdef SAMD51
        && pin->number != PIN_PA05
    #endif
    ) {
        mp_raise_ValueError(translate("AnalogOut not supported on given pin"));
        return;
    }

    self->cb->channel = 0;
    #ifdef SAMD51
    if (pin->number == PIN_PA05) {
        self->cb->channel = 1;
    }
    #endif

    #ifdef SAMD51
    hri_mclk_set_APBDMASK_DAC_bit(MCLK);
    #endif

    #ifdef SAMD21
    _pm_enable_bus_clock(PM_BUS_APBC, DAC);
    #endif

    // SAMD21: This clock should be <= 12 MHz, per datasheet section 47.6.3.
    // SAMD51: This clock should be <= 350kHz, per datasheet table 37-6.
    _gclk_enable_channel(DAC_GCLK_ID, CONF_GCLK_DAC_SRC);


    // Don't double init the DAC on the SAMD51 when both outputs are in use. We use the free state
    // of each output pin to determine DAC state.
    int32_t result = ERR_NONE;
    #ifdef SAMD51
    if (!common_hal_mcu_pin_is_free(&pin_PA02) || !common_hal_mcu_pin_is_free(&pin_PA05)) {
    #endif
        // Fake the descriptor if the DAC is already initialized.
        self->cb->descriptor.device.hw = DAC;
    #ifdef SAMD51
    } else {
    #endif
        result = dac_sync_init(&(self->cb->descriptor), DAC);
    #ifdef SAMD51
    }
    #endif
    if (result != ERR_NONE) {
        mp_raise_OSError(MP_EIO);
        return;
    }
    claim_pin(pin);


    gpio_set_pin_function(pin->number, GPIO_PIN_FUNCTION_B);

    dac_sync_enable_channel(&(self->cb->descriptor), self->cb->channel);
    DAC->DACCTRL[self->cb->channel].reg |= DAC_DACCTRL_CCTRL_CC12M;

    #endif
}

bool common_hal_arf_arfout_deinited(arf_arfout_obj_t *self) {
    return self->deinited;
}

void common_hal_arf_arfout_deinit(arf_arfout_obj_t *self) {
    #if (defined(SAMD21) && defined(PIN_PA02)) || defined(SAMD51)
    if (common_hal_arf_arfout_deinited(self)) {
        return;
    }
    dac_sync_disable_channel(&(self->cb->descriptor), self->cb->channel);
    reset_pin_number(PIN_PA02);
    // Only deinit the DAC on the SAMD51 if both outputs are free.
    #ifdef SAMD51
    if (common_hal_mcu_pin_is_free(&pin_PA02) && common_hal_mcu_pin_is_free(&pin_PA05)) {
    #endif
        dac_sync_deinit(&(self->cb->descriptor));
    #ifdef SAMD51
    }
    #endif
    self->deinited = true;
    #endif

    refcount--;
    tc_reset(tc_insts[self->tc_index]);

}

void common_hal_arf_arfout_send(arf_arfout_obj_t* self, uint16_t* wave, uint16_t length) {
    self->cb->active = true;
    self->cb->wave_buffer = wave;
    self->cb->sample_index = 0;
    self->cb->wave_length = length;

    self->cb->current_compare = self->cb->sampling_period;
    Tc* tc = tc_insts[self->tc_index];
    tc->COUNT16.CC[0].reg = self->cb->current_compare;

    // Clear our interrupt in case it was set earlier
    tc->COUNT16.INTFLAG.reg = TC_INTFLAG_MC0;
    tc->COUNT16.INTENSET.reg = TC_INTENSET_MC0;
    tc_enable_interrupts(self->tc_index);

    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_RETRIGGER;



/*    while(pulse_index < length) {
        // Do other things while we wait. The interrupts will handle sending the
        // signal.
        #ifdef MICROPY_VM_HOOK_LOOP
            MICROPY_VM_HOOK_LOOP
        #endif
    }



    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_STOP;
    tc->COUNT16.INTENCLR.reg = TC_INTENCLR_MC0;
    tc_disable_interrupts(pulseout_tc_index);
    active_pincfg = NULL;

*/
}

void common_hal_arf_arfout_set_sampling_period(arf_arfout_obj_t *self, unsigned int value) {
    self->cb->sampling_period = value;
}

unsigned int common_hal_arf_arfout_get_sampling_period(arf_arfout_obj_t *self) {
    return self->cb->sampling_period;
}

