//
// Created by DEVNET\hutchinsong on 6/5/19.
//

#ifndef MICROPYTHON_INCLUDED_ATMEL_SAMD_COMMON_HAL_ARF_ARFOUT_H
#define MICROPYTHON_INCLUDED_ATMEL_SAMD_COMMON_HAL_ARF_ARFOUT_H

#include "common-hal/microcontroller/Pin.h"
#include "hal/include/hal_dac_sync.h"

#include "py/obj.h"

struct callback_info {
    bool active;
    uint8_t channel;
    uint16_t *wave_buffer;
    volatile uint16_t sample_index;
    uint16_t wave_length;
    unsigned int sampling_period;
    volatile unsigned int current_compare;
    struct dac_sync_descriptor descriptor;
} callback_info;


typedef struct {
    mp_obj_base_t base;
    struct callback_info *cb; //references a module-static table of info for interrupts to call back to, indexed by timer index.
    bool deinited;
    uint8_t tc_index;
} arf_arfout_obj_t;

void arfout_reset(void);
void arfout_interrupt_handler(uint8_t index);

#endif //CIRCUITPYTHON_DSP_ARFOUT_H
