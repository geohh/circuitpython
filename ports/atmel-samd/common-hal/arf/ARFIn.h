//
// Created by DEVNET\hutchinsong on 6/6/19.
//

#ifndef MICROPYTHON_INCLUDED_ATMEL_SAMD_COMMON_HAL_ARFIN_H
#define MICROPYTHON_INCLUDED_ATMEL_SAMD_COMMON_HAL_ARFIN_H

#include "common-hal/microcontroller/Pin.h"
#include "hal/include/hal_adc_sync.h"

#include <stdint.h>

#include "py/obj.h"

struct arf_arfin_callback_info {
    bool dirty;
    uint8_t channel;
    uint16_t *wave_buffer;
    volatile uint16_t sample_index;
    uint16_t wave_length;
    unsigned int sampling_period;
    volatile unsigned int current_compare;
    struct adc_sync_descriptor descriptor;
} arf_arfin_callback_info;

typedef struct {
    mp_obj_base_t base;
    Adc* instance;
    bool deinited;
    uint8_t tc_index;
    struct arf_arfin_callback_info *cb;
} arf_arfin_obj_t;

//////uint16_t read_value(uint16_t *value_ptr, uint8_t channel, struct adc_sync_descriptor *descriptor);

void arfin_reset(void);
void arfin_interrupt_handler(uint8_t index);

#endif //CIRCUITPYTHON_DSP_ARFIN_H
